import importlib.resources
import pytest

from enhsp_wrapper.enhsp import ENHSP, PlanningStatus


def test_no_timeout_warning():
    with pytest.warns(
        RuntimeWarning,
        match="No timeout specified. ENHSP will "
        "run forever if the problem is unsolvable.",
    ):
        planner = ENHSP("")


def test_simple_planning():
    planner = ENHSP("-timeout 1 -s gbfs -h hadd")

    with importlib.resources.path("tests.data", "counter_domain.pddl") as domain:
        with importlib.resources.path("tests.data", "counter_small.pddl") as problem:
            result = planner.plan(str(domain), str(problem))
            assert result.status == PlanningStatus.SUCCESS
            assert result.time < 1
            assert result.stderr == ""
            assert result.stdout != ""


def test_plan_from_text():
    planner = ENHSP("-timeout 1 -s gbfs -h hadd")

    with importlib.resources.path("tests.data", "counter_domain.pddl") as domain:
        with importlib.resources.path("tests.data", "counter_small.pddl") as problem:
            with open(domain, "r") as f:
                domain_text = f.read()
            with open(problem, "r") as f:
                problem_text = f.read()
            result = planner.plan_from_string(domain_text, problem_text)
            assert result.status == PlanningStatus.SUCCESS
            assert result.time < 1


def test_unsolvable():
    planner = ENHSP("-timeout 1 -s gbfs -h hadd")

    with importlib.resources.path("tests.data", "rover_domain.pddl") as domain:
        with importlib.resources.path("tests.data", "rover_unsolvable.pddl") as problem:
            result = planner.plan(str(domain), str(problem))
            assert result.status == PlanningStatus.UNSOLVABLE
            assert result.time < 1

def test_unsolvable_ha():
    planner = ENHSP("-timeout 10 -s gbfs -h hadd -ha true")

    with importlib.resources.path("tests.data", "sugar_domain.pddl") as domain:
        with importlib.resources.path("tests.data", "sugar_ha_unsolvable.pddl") as problem:
            result = planner.plan(str(domain), str(problem))
            assert result.status == PlanningStatus.UNSOLVABLE
            assert result.time < 10

def test_timeout():
    planner = ENHSP("-timeout 1 -s gbfs -h hadd")

    with importlib.resources.path("tests.data", "rover_domain.pddl") as domain:
        with importlib.resources.path("tests.data", "rover_timeout.pddl") as problem:
            result = planner.plan(str(domain), str(problem))
            assert result.status == PlanningStatus.TIMEOUT
            assert result.time <= 1.5